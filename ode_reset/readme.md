## ENTTEC ODE Reset ##

An overly simple python script for resetting ENTTEC ODEs.  Primarily useful when you need to clear HTP data.

Written originally for the CMU Hunt Library Media Lab in 2015.  

You will need to know the MAC address of the ODE(s) you are trying to reset.

Comments and pull requests welcome.
