#!/usr/bin/python


## resetODE-UDP.py
## Resets ENNTEC ODEs
## Written by Ben Peoples (ben@benpeoples.com) but largely based on google searches.
## Many thanks to ENTTEC for example software that let us sniff out what we needed to do.
## Released into the Public Domain.


import socket
import struct

## UDP setup
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) 
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
      
## ESZZ => Reset ODE


## This resets the first ODE
data = "ESZZ"

## This is the MAC address of the ODE, in two segments.
## 0x0050C207 => 00:50:C2:07
## 0x9EB9 => 9E:B9
## The actual MAC is 00:50:C2:07:9E:B9
## You don't need to know the IP address if you're broadcasting this way.

## This was coded this way for laziness, pull requests welcome.

# append first 4 bytes
data += struct.pack("I>",0x0050C207)  
# append last 2 bytes
data += struct.pack("H>",0x9EB9)
              
sock.sendto(data, ("255.255.255.255", 3330))
sock.sendto(data, ("255.255.255.255", 3333))

## This resets the second ODE, delete if you only have one, add more if you have more than one
data = "ESZZ"
data += struct.pack("I>",0x0050C207)
data += struct.pack("H>",0xA4D9)
              
sock.sendto(data, ("255.255.255.255", 3330))
sock.sendto(data, ("255.255.255.255", 3333))

