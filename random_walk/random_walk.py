#!/usr/bin/env python

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Based on: ola_simple_fade.py Copyright (C) 2014 Sean Sill
#
# This version (c) 2015 Ben Peoples
#

# Dependencies
import time
import datetime
import pytz
import ephem

daytime = False;

# Choose your location for sunrise/sunset calculations                          
MY_TIMEZONE = "UTC"
MY_LONGITUDE = '40.4397'  # +N
MY_LATITUDE = '-80.0' # +E
MY_ELEVATION = 0          # meters   

here = ephem.Observer()                                                                                                          

from array import *
from ola.ClientWrapper import ClientWrapper
from ola.DMXConstants import DMX_MIN_SLOT_VALUE, DMX_MAX_SLOT_VALUE, \
  DMX_UNIVERSE_SIZE

import random

UPDATE_INTERVAL = 25 # In ms, this comes about to ~40 frames a second
SHUTDOWN_INTERVAL = 10000 # in ms, This is 10 seconds
DMX_DATA_SIZE = 256
UNIVERSE = 0

class SimpleFadeController(object):
  def __init__(self, universe, update_interval, client_wrapper,
               dmx_data_size=DMX_UNIVERSE_SIZE):
    dmx_data_size = min(dmx_data_size, DMX_UNIVERSE_SIZE)
    self._universe = universe
    self._update_interval = update_interval
    self._data = array ('B', [DMX_MIN_SLOT_VALUE] * dmx_data_size)
    self._wrapper = client_wrapper
    self._client = client_wrapper.Client()
    self._wrapper.AddEvent(self._update_interval, self.UpdateDmx)
    self._wrapper.AddEvent(1000, self.checktime)
    self.daytime = False;

  def UpdateDmx(self):
    """
    This function gets called periodically based on UPDATE_INTERVAL
    """
    if self.daytime:
        for i in range(len(self._data)):
          if(self._data[i] < 25):
              self._data[i] = self._data[i] + random.randrange(0,20)
          elif(self._data[i] > 230):
              self._data[i] = self._data[i] + random.randrange(-20,0)
          else:
              self._data[i] = self._data[i] + random.randrange(-25, 25)
    else:
         for i in range(len(self._data)):
          self._data[i] = (0) % DMX_MAX_SLOT_VALUE       
    # Send the DMX data
    self._client.SendDmx(self._universe, self._data)
    # For more information on Add Event, reference the OlaClient
    # Add our event again so it becomes periodic
    self._wrapper.AddEvent(self._update_interval, self.UpdateDmx)


  def checktime(self):
        self._wrapper.AddEvent(60000, self.checktime)
        s = ephem.Sun()
        sf = ephem.Observer()
	sf.lon = '-80.0'
	sf.lat = '40.4397'
	sf.elevation = 300
        s.compute(sf)
        twilight = 0
        if (s.alt > twilight):
            self.daytime = True;
        else:
            self.daytime = False;
        return 0
    

def main():                                                                     
  # Configure the timezone                                                      
  pytz.timezone(MY_TIMEZONE)                                                                 

  # Configure the ephem object
  here.lat = MY_LATITUDE                                          
  here.lon = MY_LONGITUDE                                                   
  here.elevation = MY_ELEVATION 
  wrapper = ClientWrapper()
  controller = SimpleFadeController(UNIVERSE, UPDATE_INTERVAL, wrapper,
                                      DMX_DATA_SIZE)
  wrapper.Run()

                                                             

if __name__ == '__main__':                                                      
  main()
  
