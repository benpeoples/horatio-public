### Random Walk

With code largely re-used from an OLA example, this is a simple implementation of a random-walk algorithm for lighting.

It random-walks 256 DMX channels between 0 and 255 without really paying much attention to what it's doing.

It also detects when the sun is up and only runs the lighting during the day, however the transition is pretty sharp.

See blog post on [Ben Peoples](http://benpeoples.com) for more.